import {expect} from 'chai'
import locations from "@/store/modules/locations";
const mutations = locations.mutations;

const {resetLocations, setLocationByIndex, setLocation} = mutations;

describe('mutations', () => {
    it('Test: setLocationByIndex', function () {
        const state = {
            allLocations: [{
                "geometry": {
                    "location": {
                        "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                        "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    },
                    "viewport": {
                        "pa": {"g": 52.5180556697085, "h": 52.5207536302915},
                        "ka": {"g": 13.401619269708476, "h": 13.404317230291554}
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                "id": "73cdf37e1b1e6259081acd57c24da72fff6fceb7",
                "name": "Radisson Blu Hotel, Berlin",
                "opening_hours": {
                    "open_now": true,
                    "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}
                },
                "photos": [{
                    "height": 381,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114956845246193678600\">Radisson Blu Hotel, Berlin</a>"],
                    "width": 678,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                }],
                  "place_id": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                  "plus_code": {"compound_code": "GC93+R3 Berlin, Germany", "global_code": "9F4MGC93+R3"},
                  "rating": 4.4,
                  "reference": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                  "scope": "GOOGLE",
                  "types": ["lodging", "bar", "restaurant", "food", "gym", "health", "point_of_interest", "establishment"],
                  "user_ratings_total": 2273,
                  "vicinity": "Karl-Liebknecht-Straße 3, Berlin",
                  "html_attributions": [],
                  "active": true
                }, {
                  "geometry": {
                    "location": {
                      "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                      "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    }, "viewport": {"pa": {"g": 52.5208012, "h": 52.524918}, "ka": {"g": 13.410704200000055, "h": 13.4165706}}
                  },
                  "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                  "id": "343fa2c18b7a06d127c1226736f7c9fc29e7616a",
                  "name": "Park Inn by Radisson Berlin Alexanderplatz Hotel",
                  "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
                  "photos": [{
                    "height": 574,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/108578655026207443751\">Park Inn by Radisson Berlin Alexanderplatz Hotel</a>"],
                    "width": 1021,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                  }],"place_id": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                  "plus_code": {"compound_code": "GCF7+45 Berlin, Germany", "global_code": "9F4MGCF7+45"},
                  "rating": 4.1,
                  "reference": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                  "scope": "GOOGLE",
                  "types": ["lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
                  "user_ratings_total": 11762,
                  "vicinity": "Alexanderplatz 7, Berlin",
                  "html_attributions": [],
                  "active": false
                }]
        };

        setLocationByIndex(state, {index: 0, active: false});
        expect(state.allLocations[0].active).to.be.false;

        setLocationByIndex(state, {index: 0, active: false});
        expect(state.allLocations[0].active).to.be.false;

        setLocationByIndex(state, {index: 0, active: true});
        expect(state.allLocations[0].active).to.be.true;

        setLocationByIndex(state, {index: 0, active: true});
        expect(state.allLocations[0].active).to.be.true;


    });
    it('Test: setLocation', function () {
        const state = {
            allLocations: [{
                "geometry": {
                    "location": {
                        "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                        "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    },
                    "viewport": {
                        "pa": {"g": 52.5180556697085, "h": 52.5207536302915},
                        "ka": {"g": 13.401619269708476, "h": 13.404317230291554}
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                "id": "73cdf37e1b1e6259081acd57c24da72fff6fceb7",
                "name": "Radisson Blu Hotel, Berlin",
                "opening_hours": {
                    "open_now": true,
                    "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}
                },
                "photos": [{
                    "height": 381,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114956845246193678600\">Radisson Blu Hotel, Berlin</a>"],
                    "width": 678,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                }],
                "place_id": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                "plus_code": {"compound_code": "GC93+R3 Berlin, Germany", "global_code": "9F4MGC93+R3"},
                "rating": 4.4,
                "reference": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                "scope": "GOOGLE",
                "types": ["lodging", "bar", "restaurant", "food", "gym", "health", "point_of_interest", "establishment"],
                "user_ratings_total": 2273,
                "vicinity": "Karl-Liebknecht-Straße 3, Berlin",
                "html_attributions": [],
                "active": true
            }, {
                "geometry": {
                    "location": {
                        "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                        "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    }, "viewport": {"pa": {"g": 52.5208012, "h": 52.524918}, "ka": {"g": 13.410704200000055, "h": 13.4165706}}
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                "id": "343fa2c18b7a06d127c1226736f7c9fc29e7616a",
                "name": "Park Inn by Radisson Berlin Alexanderplatz Hotel",
                "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
                "photos": [{
                    "height": 574,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/108578655026207443751\">Park Inn by Radisson Berlin Alexanderplatz Hotel</a>"],
                    "width": 1021,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                }],"place_id": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                "plus_code": {"compound_code": "GCF7+45 Berlin, Germany", "global_code": "9F4MGCF7+45"},
                "rating": 4.1,
                "reference": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                "scope": "GOOGLE",
                "types": ["lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
                "user_ratings_total": 11762,
                "vicinity": "Alexanderplatz 7, Berlin",
                "html_attributions": [],
                "active": false
            }]
        };

        setLocation(state, {location: state.allLocations[1], active: true});
        expect(state.allLocations[1].active).to.be.true;

        setLocation(state, {location: state.allLocations[1], active: true});
        expect(state.allLocations[1].active).to.be.true;

        setLocation(state, {location: state.allLocations[1], active: false});
        expect(state.allLocations[1].active).to.be.false;

        setLocation(state, {location: state.allLocations[1], active: false});
        expect(state.allLocations[1].active).to.be.false;


    });
    it('Test: resetLocations', function () {
        const state = {
            allLocations: [{
                "geometry": {
                    "location": {
                        "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                        "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    },
                    "viewport": {
                        "pa": {"g": 52.5180556697085, "h": 52.5207536302915},
                        "ka": {"g": 13.401619269708476, "h": 13.404317230291554}
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                "id": "73cdf37e1b1e6259081acd57c24da72fff6fceb7",
                "name": "Radisson Blu Hotel, Berlin",
                "opening_hours": {
                    "open_now": true,
                    "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}
                },
                "photos": [{
                    "height": 381,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114956845246193678600\">Radisson Blu Hotel, Berlin</a>"],
                    "width": 678,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                }],
                "place_id": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                "plus_code": {"compound_code": "GC93+R3 Berlin, Germany", "global_code": "9F4MGC93+R3"},
                "rating": 4.4,
                "reference": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
                "scope": "GOOGLE",
                "types": ["lodging", "bar", "restaurant", "food", "gym", "health", "point_of_interest", "establishment"],
                "user_ratings_total": 2273,
                "vicinity": "Karl-Liebknecht-Straße 3, Berlin",
                "html_attributions": [],
                "active": true
            }, {
                "geometry": {
                    "location": {
                        "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                        "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                    }, "viewport": {"pa": {"g": 52.5208012, "h": 52.524918}, "ka": {"g": 13.410704200000055, "h": 13.4165706}}
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                "id": "343fa2c18b7a06d127c1226736f7c9fc29e7616a",
                "name": "Park Inn by Radisson Berlin Alexanderplatz Hotel",
                "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
                "photos": [{
                    "height": 574,
                    "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/108578655026207443751\">Park Inn by Radisson Berlin Alexanderplatz Hotel</a>"],
                    "width": 1021,
                    "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
                }],"place_id": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                "plus_code": {"compound_code": "GCF7+45 Berlin, Germany", "global_code": "9F4MGCF7+45"},
                "rating": 4.1,
                "reference": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
                "scope": "GOOGLE",
                "types": ["lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
                "user_ratings_total": 11762,
                "vicinity": "Alexanderplatz 7, Berlin",
                "html_attributions": [],
                "active": false
            }]
        };

        resetLocations(state);
        expect(state.allLocations).to.be.empty;
    });
});