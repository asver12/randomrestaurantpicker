import {expect} from 'chai'
import {shallowMount} from "@vue/test-utils";
import LocationCard from "@/components/LocationCard";

describe("LocationCard", () => {
    it("LocationCardAttributesExist", () => {
        const loca = {
            "geometry": {
                "location": {
                    "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
                    "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
                },
                "viewport": {
                    "pa": {"g": 52.5261055697085, "h": 52.5288035302915},
                    "ka": {"g": 13.414225769708537, "h": 13.4169237302915}
                }
            },
            "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
            "id": "670d6db02d3aebef503c213017898230b8dde584",
            "name": "Soho House Berlin",
            "opening_hours": {
                "open_now": true,
                "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}
            },
            "photos": [{
                "height": 720,
                "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/105836471432848982885\">Soho House Berlin</a>"],
                "width": 960,
                "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
            }],
            "place_id": "ChIJ7QGeQxxOqEcRFvDmifxqZO8",
            "plus_code": {"compound_code": "GCH8+27 Berlin, Deutschland", "global_code": "9F4MGCH8+27"},
            "rating": 4.4,
            "reference": "ChIJ7QGeQxxOqEcRFvDmifxqZO8",
            "scope": "GOOGLE",
            "types": ["spa", "lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
            "user_ratings_total": 1794,
            "vicinity": "Torstraße 1, Berlin",
            "html_attributions": [],
            "active": true
        };
        const wrapper = shallowMount(LocationCard, {
            propsData: {loca}
        });
        expect(wrapper.find(".location-item").exists()).to.true;
        expect(wrapper.text()).to.include(loca.name);
        expect(wrapper.text()).to.include("Open");
        expect(wrapper.text()).to.include(loca.rating);
    })
});