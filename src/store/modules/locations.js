function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

function checkActive(locationsList) {
  return locationsList.active
}

const state = {
  userLocation: {lat: 52.520, lng: 13.405},
  isSelected: null,
  idforLocation: 9,
  allLocations: [],
  //     [{
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5180556697085, "h": 52.5207536302915},
  //       "ka": {"g": 13.401619269708476, "h": 13.404317230291554}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "73cdf37e1b1e6259081acd57c24da72fff6fceb7",
  //   "name": "Radisson Blu Hotel, Berlin",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 381,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114956845246193678600\">Radisson Blu Hotel, Berlin</a>"],
  //     "width": 678,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
  //   "plus_code": {"compound_code": "GC93+R3 Berlin, Germany", "global_code": "9F4MGC93+R3"},
  //   "rating": 4.4,
  //   "reference": "ChIJLTX2ud9RqEcRtiblhC9s3Rg",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "bar", "restaurant", "food", "gym", "health", "point_of_interest", "establishment"],
  //   "user_ratings_total": 2273,
  //   "vicinity": "Karl-Liebknecht-Straße 3, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     }, "viewport": {"pa": {"g": 52.5208012, "h": 52.524918}, "ka": {"g": 13.410704200000055, "h": 13.4165706}}
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "343fa2c18b7a06d127c1226736f7c9fc29e7616a",
  //   "name": "Park Inn by Radisson Berlin Alexanderplatz Hotel",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 574,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/108578655026207443751\">Park Inn by Radisson Berlin Alexanderplatz Hotel</a>"],
  //     "width": 1021,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
  //   "plus_code": {"compound_code": "GCF7+45 Berlin, Germany", "global_code": "9F4MGCF7+45"},
  //   "rating": 4.1,
  //   "reference": "ChIJ4Q7iHh9OqEcRoYdM_f1daq0",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 11762,
  //   "vicinity": "Alexanderplatz 7, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.51162996970849, "h": 52.51432793029149},
  //       "ka": {"g": 13.403695019708493, "h": 13.406392980291457}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "973e6e13b0f0cf6e845962f6bba43dee88384662",
  //   "name": "Hotel Novotel Berlin Mitte",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 768,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/111482740722870294072\">Hotel Novotel Berlin Mitte</a>"],
  //     "width": 1024,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJR5kGoSdOqEcRCiB4N4CC5rw",
  //   "plus_code": {"compound_code": "GC74+62 Berlin, Germany", "global_code": "9F4MGC74+62"},
  //   "rating": 4.3,
  //   "reference": "ChIJR5kGoSdOqEcRCiB4N4CC5rw",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1837,
  //   "vicinity": "Fischerinsel 12, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5261055697085, "h": 52.5288035302915},
  //       "ka": {"g": 13.414225769708537, "h": 13.4169237302915}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "670d6db02d3aebef503c213017898230b8dde584",
  //   "name": "Soho House Berlin",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 720,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/105836471432848982885\">Soho House Berlin</a>"],
  //     "width": 960,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ7QGeQxxOqEcRFvDmifxqZO8",
  //   "plus_code": {"compound_code": "GCH8+27 Berlin, Germany", "global_code": "9F4MGCH8+27"},
  //   "rating": 4.4,
  //   "reference": "ChIJ7QGeQxxOqEcRFvDmifxqZO8",
  //   "scope": "GOOGLE",
  //   "types": ["spa", "lodging", "bar", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1742,
  //   "vicinity": "Torstraße 1, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.52332971970851, "h": 52.52602768029151},
  //       "ka": {"g": 13.418594319708518, "h": 13.421292280291482}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "e75ac18d108e6ff500d4b8ab077dadb77ce64f0c",
  //   "name": "Mercure Hotel Berlin am Alexanderplatz",
  //   "photos": [{
  //     "height": 2268,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/109363386951651084823\">Dikte Martinsen</a>"],
  //     "width": 4032,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJNQRlghlOqEcRLEEvCjwpHkA",
  //   "plus_code": {"compound_code": "GCFC+R3 Berlin, Germany", "global_code": "9F4MGCFC+R3"},
  //   "rating": 3.6,
  //   "reference": "ChIJNQRlghlOqEcRLEEvCjwpHkA",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1244,
  //   "vicinity": "Mollstraße 4, Berlin",
  //   "html_attributions": [],
  //   "opening_hours": {"open_now": true},
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5102734697085, "h": 52.5129714302915},
  //       "ka": {"g": 13.399868219708537, "h": 13.402566180291501}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "5ff9634b87ec4f0d516f3c5241b4895f686d10a7",
  //   "name": "Cosmo Hotel Berlin Mitte",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 3288,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/116276552500531863874\">Marcus A. N.</a>"],
  //     "width": 4384,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ77m8D9hRqEcRY5NPTAq9MF0",
  //   "plus_code": {"compound_code": "GC62+JH Berlin, Germany", "global_code": "9F4MGC62+JH"},
  //   "rating": 4.4,
  //   "reference": "ChIJ77m8D9hRqEcRY5NPTAq9MF0",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 524,
  //   "vicinity": "Spittelmarkt 13, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.52089451970849, "h": 52.52359248029149},
  //       "ka": {"g": 13.40289006970852, "h": 13.405588030291483}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "5f1037a9e9ac39a1fe9edf22b6553f2d6980babc",
  //   "name": "Adina Apartment Hotel Berlin Hackescher Markt",
  //   "photos": [{
  //     "height": 2160,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114940419128781434138\">Greta Lun</a>"],
  //     "width": 3840,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJNTJq9OBRqEcR3jx__4NZNuk",
  //   "plus_code": {"compound_code": "GCC3+VM Berlin, Germany", "global_code": "9F4MGCC3+VM"},
  //   "rating": 4.6,
  //   "reference": "ChIJNTJq9OBRqEcR3jx__4NZNuk",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 306,
  //   "vicinity": "An der Spandauer Brücke 11, Berlin",
  //   "html_attributions": [],
  //   "opening_hours": {"open_now": true},
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5289508697085, "h": 52.5316488302915},
  //       "ka": {"g": 13.380355919708563, "h": 13.383053880291527}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "92065964cb40f26f1833700913a826e492e6ffb2",
  //   "name": "Mercure Hotel Berlin City",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 2414,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/101303282226988090945\">Mercure Hotel Berlin City</a>"],
  //     "width": 3620,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJaY2DvOxRqEcRL4lYub-pBBY",
  //   "plus_code": {"compound_code": "G9JJ+5M Berlin, Germany", "global_code": "9F4MG9JJ+5M"},
  //   "rating": 4.3,
  //   "reference": "ChIJaY2DvOxRqEcRL4lYub-pBBY",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1326,
  //   "vicinity": "Invalidenstraße 38, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.51024046970849, "h": 52.51293843029149},
  //       "ka": {"g": 13.406337169708536, "h": 13.4090351302915}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "90cbd0884b667f0a0350fc499419b52f52db6346",
  //   "name": "Park Plaza Wallstreet Berlin Mitte",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 3788,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/112193870674488047806\">Park Plaza Wallstreet Berlin Mitte</a>"],
  //     "width": 5051,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJDz9uQCZOqEcRg8fnjtyf31k",
  //   "plus_code": {"compound_code": "GC65+J3 Berlin, Germany", "global_code": "9F4MGC65+J3"},
  //   "rating": 4.3,
  //   "reference": "ChIJDz9uQCZOqEcRg8fnjtyf31k",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1006,
  //   "vicinity": "Wallstraße 23-24, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5130815197085, "h": 52.5157794802915},
  //       "ka": {"g": 13.389752669708514, "h": 13.392450630291478}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "8e7a54ff6c14fa27b8013741272bcd6cdd01ab37",
  //   "name": "Hotel Berlin Gendarmenmarkt",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 2448,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/115968037769023344203\">Jörg Lo</a>"],
  //     "width": 3264,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ4TPH-hQHpEcR0b6Qmo_aK7Q",
  //   "plus_code": {"compound_code": "G97R+QC Berlin, Germany", "global_code": "9F4MG97R+QC"},
  //   "rating": 4.5,
  //   "reference": "ChIJ4TPH-hQHpEcR0b6Qmo_aK7Q",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 312,
  //   "vicinity": "Charlottenstraße 50-52, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5283959197085, "h": 52.5310938802915},
  //       "ka": {"g": 13.423730119708466, "h": 13.426428080291544}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
  //   "id": "496f190eeb1ad35168384dfe3ffb6faf43b39fc4",
  //   "name": "Adele Designhotel Berlin",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1114,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/113874449035803137791\">Adele Designhotel Berlin</a>"],
  //     "width": 854,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJHz3rtRpOqEcRzjWPk47-sb8",
  //   "plus_code": {"compound_code": "GCHF+WX Berlin, Germany", "global_code": "9F4MGCHF+WX"},
  //   "rating": 4.1,
  //   "reference": "ChIJHz3rtRpOqEcRzjWPk47-sb8",
  //   "scope": "GOOGLE",
  //   "types": ["lodging", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 86,
  //   "vicinity": "Greifswalder Straße 227, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5212877697085, "h": 52.5239857302915},
  //       "ka": {"g": 13.386799919708437, "h": 13.389497880291515}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "f980a6d0f26e2ecea0adb814bdc9f9149c65d8d1",
  //   "name": "Grill Royal",
  //   "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1341,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/116224190109023888939\">Grill Royal</a>"],
  //     "width": 2048,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJj4s6fcJRqEcR5T05XzNpwLA",
  //   "plus_code": {"compound_code": "G9FQ+29 Berlin, Germany", "global_code": "9F4MG9FQ+29"},
  //   "price_level": 4,
  //   "rating": 4.2,
  //   "reference": "ChIJj4s6fcJRqEcR5T05XzNpwLA",
  //   "scope": "GOOGLE",
  //   "types": ["bar", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1187,
  //   "vicinity": "Friedrichstraße 105b, Berlin",
  //   "html_attributions": [],
  //   "active": false
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5165048197085, "h": 52.5192027802915},
  //       "ka": {"g": 13.386470769708467, "h": 13.38916873029143}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "e8c5bcd35a89aaae834b251ed012ac159bffc00a",
  //   "name": "Vapiano",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 800,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/109194441111662174915\">VAPIANO | Berlin | Mittelstraße</a>"],
  //     "width": 1200,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJo1XeYcNRqEcRQUhUnA5jei0",
  //   "plus_code": {"compound_code": "G99Q+34 Berlin, Germany", "global_code": "9F4MG99Q+34"},
  //   "price_level": 2,
  //   "rating": 3.8,
  //   "reference": "ChIJo1XeYcNRqEcRQUhUnA5jei0",
  //   "scope": "GOOGLE",
  //   "types": ["bar", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 2741,
  //   "vicinity": "Mittelstraße 51-52, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5160189697085, "h": 52.5187169302915},
  //       "ka": {"g": 13.412389719708472, "h": 13.415087680291435}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "394abd194848f043589b352c3dc23c4840b811f8",
  //   "name": "Zur letzten Instanz",
  //   "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1678,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/108523103094738835758\">Zur Letzten Instanz</a>"],
  //     "width": 2048,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ1WEbzyNOqEcRKqrOGWDKPMs",
  //   "plus_code": {"compound_code": "GC87+WG Berlin, Germany", "global_code": "9F4MGC87+WG"},
  //   "price_level": 2,
  //   "rating": 4.1,
  //   "reference": "ChIJ1WEbzyNOqEcRKqrOGWDKPMs",
  //   "scope": "GOOGLE",
  //   "types": ["bar", "restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1854,
  //   "vicinity": "Waisenstraße 14-16, Berlin",
  //   "html_attributions": [],
  //   "active": false
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5145058197085, "h": 52.5172037802915},
  //       "ka": {"g": 13.38687046970847, "h": 13.389568430291547}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "a2f30c28b1dab6952f131fb440e2fed448ba0bf6",
  //   "name": "Cookies Cream",
  //   "opening_hours": {"open_now": false, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 2480,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/112603944009632014672\">Cookies Cream</a>"],
  //     "width": 3508,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJV4_rtsRRqEcRIKoaNDZ0VnQ",
  //   "plus_code": {"compound_code": "G98Q+F7 Berlin, Germany", "global_code": "9F4MG98Q+F7"},
  //   "rating": 4.5,
  //   "reference": "ChIJV4_rtsRRqEcRIKoaNDZ0VnQ",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 648,
  //   "vicinity": "Behrenstraße 55, Berlin",
  //   "html_attributions": [],
  //   "active": false
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.52326701970849, "h": 52.52596498029149},
  //       "ka": {"g": 13.41216421970853, "h": 13.414862180291493}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "064e7b67da446949ebce13d7992187c567768422",
  //   "name": "Hofbräu Wirtshaus Berlin",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1280,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/104297580745549051899\">Jörg Unkel</a>"],
  //     "width": 1920,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJA3tAkR5OqEcRHQ3BP-tASds",
  //   "plus_code": {"compound_code": "GCF7+QG Berlin, Germany", "global_code": "9F4MGCF7+QG"},
  //   "price_level": 2,
  //   "rating": 4.2,
  //   "reference": "ChIJA3tAkR5OqEcRHQ3BP-tASds",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 13825,
  //   "vicinity": "Karl-Liebknecht-Straße 30, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5191721697085, "h": 52.5218701302915},
  //       "ka": {"g": 13.392751419708475, "h": 13.395449380291439}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "ddb9ea9ffaa36a1ca97bc9dc30690ad280803d8b",
  //   "name": "12 Apostel Berlin Mitte",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 747,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/107025667034753101613\">Niko Cataldi</a>"],
  //     "width": 1066,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ5xzwD91RqEcR_luSuyvVUNY",
  //   "plus_code": {"compound_code": "G9CV+6M Berlin, Germany", "global_code": "9F4MG9CV+6M"},
  //   "price_level": 2,
  //   "rating": 4.1,
  //   "reference": "ChIJ5xzwD91RqEcR_luSuyvVUNY",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1067,
  //   "vicinity": "S-Bahnbögen 177-180, Georgenstraße 2, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5135494197085, "h": 52.5162473802915},
  //       "ka": {"g": 13.388971419708469, "h": 13.391669380291432}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "afb53211c457d469ccf9e2af9a27af0969ea2136",
  //   "name": "borchardt",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1200,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/114024187048890912514\">Borchardt</a>"],
  //     "width": 1800,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJVfEmINtRqEcRsch0pslaanQ",
  //   "plus_code": {"compound_code": "G97R+X5 Berlin, Germany", "global_code": "9F4MG97R+X5"},
  //   "price_level": 3,
  //   "rating": 4,
  //   "reference": "ChIJVfEmINtRqEcRsch0pslaanQ",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 1363,
  //   "vicinity": "Französische Straße 47, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5125357697085, "h": 52.51523373029149},
  //       "ka": {"g": 13.389789969708545, "h": 13.392487930291509}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "ba749d64b7a077c199aa5fb02cfe26eb807aa3cb",
  //   "name": "Augustiner am Gendarmenmarkt",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 3024,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/113599484257873980537\">Gustavo Rodríguez</a>"],
  //     "width": 4032,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJ8a9989pRqEcR6ZdZFWOKsJE",
  //   "plus_code": {"compound_code": "G97R+GC Berlin, Germany", "global_code": "9F4MG97R+GC"},
  //   "price_level": 2,
  //   "rating": 4,
  //   "reference": "ChIJ8a9989pRqEcR6ZdZFWOKsJE",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "food", "point_of_interest", "establishment"],
  //   "user_ratings_total": 4714,
  //   "vicinity": "Charlottenstraße 56, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }, {
  //   "geometry": {
  //     "location": {
  //       "lat": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}},
  //       "lng": {"_custom": {"type": "function", "display": "<span>ƒ</span> ()"}}
  //     },
  //     "viewport": {
  //       "pa": {"g": 52.5216753697085, "h": 52.5243733302915},
  //       "ka": {"g": 13.407713669708528, "h": 13.410411630291492}
  //     }
  //   },
  //   "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
  //   "id": "012c531a4fc417ba177222ab0723d4ab97c6b0fc",
  //   "name": "Spreegold Store Ros 02",
  //   "opening_hours": {"open_now": true, "isOpen": {"_custom": {"type": "function", "display": "<span>ƒ</span> (l)"}}},
  //   "photos": [{
  //     "height": 1152,
  //     "html_attributions": ["<a href=\"https://maps.google.com/maps/contrib/103821736318471354215\">Spreegold Store Ros 02</a>"],
  //     "width": 2048,
  //     "getUrl": {"_custom": {"type": "function", "display": "<span>ƒ</span> (g)"}}
  //   }],
  //   "place_id": "ChIJJdTMVw5OqEcRajmwtdn7TNU",
  //   "plus_code": {"compound_code": "GCF5+5M Berlin, Germany", "global_code": "9F4MGCF5+5M"},
  //   "price_level": 2,
  //   "rating": 3.8,
  //   "reference": "ChIJJdTMVw5OqEcRajmwtdn7TNU",
  //   "scope": "GOOGLE",
  //   "types": ["restaurant", "cafe", "food", "point_of_interest", "store", "establishment"],
  //   "user_ratings_total": 1673,
  //   "vicinity": "Rosa-Luxemburg-Straße 2, Berlin",
  //   "html_attributions": [],
  //   "active": true
  // }],
  activeList: [],
  winner: {},

};

const getters = {
  allLocations: state => {
    return state.allLocations
  },
  getWinner: state => {
    return state.winner
  },
  getUserLocation: state => {
    return state.userLocation
  },
  getGoogle: state => {
    return state.google
  },
  getLocationByIndex: (state) => (index) => {
    return state.allLocations[index];
  },
  getLocation: (state) => (location) => {
    return state.allLocations.find(x => x.id === location.id)
  }
};

const actions = {
  selectALocation({commit, dispatch, state}) {
      if (state.winner !== null && state.winner.active) {
        dispatch("disableLocation",state.winner)
      }
      const allLocations = [...state.allLocations];
      let locationsRanking = shuffle(allLocations.filter(checkActive));

      if(locationsRanking.length !== 0) {
        const winner = locationsRanking[locationsRanking.length-1];
        commit("setWinner", winner);
      } else {
        commit("setWinner", "")
      }
  },
  disableLocationByIndex(context, index) {
    const _place = state.allLocations[index];
    if (!(_place.opening_hours && _place.opening_hours.open_now !== undefined && !_place.opening_hours.open_now)){
      context.commit("setLocationByIndex", {"index": index, "active": false})
    }
  },
  enableLocationByIndex(context, index) {
    const _place = state.allLocations[index];
    if (!(_place.opening_hours && _place.opening_hours.open_now !== undefined && !_place.opening_hours.open_now)){
      context.commit("setLocationByIndex", {"index": index, "active": true})
    }
  },
  disableLocation(context, location) {
    const _place = state.allLocations.find(x => x.id === location.id);
    if (!(_place.opening_hours && _place.opening_hours.open_now !== undefined && !_place.opening_hours.open_now)){
      context.commit("setLocation", {"location": _place, "active": false})
    }
  },
  enableLocation(context, location) {
    const _place = state.allLocations.find(x => x.id === location.id);
    if (!(_place.opening_hours && _place.opening_hours.open_now !== undefined && !_place.opening_hours.open_now)){
      context.commit("setLocation", {"location": _place, "active": true})
    }
  },
};




const mutations = {
  setLocationByIndex(state, payload){
    state.allLocations[payload.index].active = payload.active
  },
  setLocation(state, payload){
    state.allLocations.find(x => x.id === payload.location.id).active = payload.active;
  },
  setWinner(state, location) {
    state.winner = location;
  },
  setUserLocation(state, location) {
    state.userLocation = location
  },
  resetLocations(state) {
    state.allLocations = []
  },
  addPlacesLocations(state, locations) {
    locations.forEach(location => {
          if (location.opening_hours && location.opening_hours.open_now !== undefined) {
            state.allLocations.push({...location, active: location.opening_hours.open_now})
            // location.opening_hours.open_now = true;
          } else {
            location.opening_hours = {open_now: true};
            state.allLocations.push({...location, active: location.opening_hours.open_now})
          }
        }
    )
  },
  addPlacesLocation(state, location) {
    if (location.opening_hours && location.opening_hours.open_now !== undefined) {
      state.allLocations.push({...location, active: location.opening_hours.open_now})
      // location.opening_hours.open_now = true;
    } else {
      location.opening_hours = {open_now: true};
      state.allLocations.push({...location, active: location.opening_hours.open_now})
    }
  },
  addLocation(state, newLocation, open = true, rating = 5.0) {
    state.allLocations.push({
      id: state.idforLocation,
      name: newLocation,
      opening_hours: {
        open_now: "true"
      },
      rating: rating,
      active: open,
    });
    state.idforLocation++
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}