const mapSettings = {
    zoom: 14,
    options: {
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: true,
        fullscreenControl: false,
        disableDefaultUi: false
    }
};

const directionsSettings = {
    suppressMarkers: true,
};

export {mapSettings, directionsSettings}