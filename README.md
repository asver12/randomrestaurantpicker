# Random Restaurant Picker

The Random Restaurant Picker offers a decision tool if you dont know where to eat today. 
The website belonging to this repository is: [rarepi](https://rarepi.com/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
